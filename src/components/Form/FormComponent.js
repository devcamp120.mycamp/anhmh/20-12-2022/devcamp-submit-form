import { Component } from "react";

class FormComponent extends Component {
    render() {
        return (
            <>
            <div className="container jumbotron" style={{ background: "lightgray", paddingTop: "50px", paddingBottom: "50px" }}>
                <div className="row mt-2">
                    <div className="col-4">
                        <label style={{ paddingLeft: "100px" }}>First Name</label>
                    </div>
                    <div className="col-8">
                        <input className="form-control" placeholder="Your firstname" style={{ paddingLeft: "30px", width: "800px" }}></input>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-4">
                        <label style={{ paddingLeft: "100px" }}>Last Name</label>
                    </div>
                    <div className="col-8">
                        <input className="form-control" placeholder="Your lastname" style={{ paddingLeft: "30px", width: "800px" }}></input>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-4">
                        <label style={{ paddingLeft: "100px" }}>Country</label>
                    </div>
                    <div className="col-8">
                        <select className="form-control" style={{ paddingLeft: "30px", width: "800px" }}>
                            <option valueName="Australia">Australia</option>
                            <option valueName="America">America</option>
                        </select>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-4">
                        <label style={{ paddingLeft: "100px" }}>Subject</label>
                    </div>
                    <div className="col-8">
                        <textarea className="form-control" placeholder="Write something..." style={{ paddingLeft: "30px", height: "350px", width: "800px" }}></textarea>
                    </div>
                </div>
                <div className="row mt-2">
                    <div style={{ paddingLeft: "100px" }} className="col-10">
                        <button style={{ background: "green", color: "lightyellow" }}>Gửi thông tin</button>
                    </div>
                </div>
            </div>
            </>
        )
    }
}

export default FormComponent;