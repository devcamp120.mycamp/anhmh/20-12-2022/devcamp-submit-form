import 'bootstrap/dist/css/bootstrap.min.css';
import FormComponent from './components/Form/FormComponent';
import TitleComponent from './components/TitleComponent/TitleComponent';


function App() {
  return (
    <div>
      <TitleComponent/>
      <FormComponent/>
    </div>
  );
}

export default App;
